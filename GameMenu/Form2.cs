﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameMenu
{
    public partial class Form2 : Form
    {


        int playerSpeed = 5;

        int p1Vel;
        int p2Vel;

        int bVelX = 3;
        int bVelY = 3;

        int p1scoreInt;
        int p2scoreInt;

        bool pause = false;

        public Form2()
        {
            InitializeComponent();
        }

        private void close_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Möchten Sie das Menü wirklich schließen?", "Schließen", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        private void player1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!pause)
            {
                ball.Location = new Point(ball.Location.X + bVelX, ball.Location.Y + bVelY);
                player1.Location = new Point(player1.Location.X + p1Vel, player1.Location.Y);
                player2.Location = new Point(player2.Location.X + p2Vel, player2.Location.Y);
            }
            if (ball.Location.Y < 0)
            {
                p1scoreInt++;
                ball.Location = new Point(this.Width / 2, this.Height / 2);

            }
            if (ball.Location.Y > this.Height)
            {
                p2scoreInt++;
                ball.Location = new Point(this.Width / 2, this.Height / 2);
            }

            if (ball.Location.X > player1.Location.X && ball.Location.X + ball.Width < player1.Location.X + player1.Width && ball.Location.Y + ball.Height > player1.Location.Y)
            {
                bVelY *= -1;
            }
            if (ball.Location.X > player2.Location.X && ball.Location.X + ball.Width < player2.Location.X + player2.Width && ball.Location.Y < player2.Location.Y + player2.Height)
            {
                bVelY *= -1;
            }
            if (ball.Location.X < 0)
            {
                bVelX *= -1;
            }
            if (ball.Location.X + ball.Width > this.Width)
            {
                bVelX *= -1;
            }

            p1score.Text = p1scoreInt.ToString();
            p2score.Text = p2scoreInt.ToString();
        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                p1Vel = playerSpeed;
            }
            else if (e.KeyCode == Keys.Left)
            {
                p1Vel = -playerSpeed;
            }
            else if (e.KeyCode == Keys.D)
            {
                p2Vel = playerSpeed;
            }
            else if (e.KeyCode == Keys.A)
            {
                p2Vel = -playerSpeed;
            }
            else if (e.KeyCode == Keys.P)
            {
                if (!pause)
                {
                    pause = true;
                }
                else if (pause)
                {
                    pause = false;
                }
            }
        }

        private void Form2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Right)
            {
                p1Vel = 0;
            }
            else if (e.KeyCode == Keys.Left)
            {
                p1Vel = 0;
            }
            else if (e.KeyCode == Keys.D)
            {
                p2Vel = 0;
            }
            else if (e.KeyCode == Keys.A)
            {
                p2Vel = 0;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void p1score_Click(object sender, EventArgs e)
        {

        }

        private void ball_Click(object sender, EventArgs e)
        {

        }

        private void player2_Click(object sender, EventArgs e)
        {
            schleich.di
        }

        private void ball_Click_1(object sender, EventArgs e) {

        }
    }
}//bbb