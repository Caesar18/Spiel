﻿namespace GameMenu
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pingpong = new System.Windows.Forms.Button();
            this.tictactoe = new System.Windows.Forms.Button();
            this.close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // pingpong
            // 
            this.pingpong.BackColor = System.Drawing.Color.White;
            this.pingpong.Location = new System.Drawing.Point(268, 91);
            this.pingpong.Name = "pingpong";
            this.pingpong.Size = new System.Drawing.Size(300, 100);
            this.pingpong.TabIndex = 0;
            this.pingpong.Text = "Ping Pong";
            this.pingpong.UseVisualStyleBackColor = false;
            this.pingpong.Click += new System.EventHandler(this.pingpong_Click);
            // 
            // tictactoe
            // 
            this.tictactoe.BackColor = System.Drawing.Color.White;
            this.tictactoe.Location = new System.Drawing.Point(268, 207);
            this.tictactoe.Name = "tictactoe";
            this.tictactoe.Size = new System.Drawing.Size(300, 100);
            this.tictactoe.TabIndex = 1;
            this.tictactoe.Text = "Tic Tac Toe";
            this.tictactoe.UseVisualStyleBackColor = false;
            this.tictactoe.Click += new System.EventHandler(this.tictactoe_Click);
            // 
            // close
            // 
            this.close.BackColor = System.Drawing.Color.White;
            this.close.Location = new System.Drawing.Point(698, 411);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(113, 37);
            this.close.TabIndex = 2;
            this.close.Text = "Schließen";
            this.close.UseVisualStyleBackColor = false;
            this.close.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(823, 460);
            this.Controls.Add(this.close);
            this.Controls.Add(this.tictactoe);
            this.Controls.Add(this.pingpong);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button pingpong;
        private System.Windows.Forms.Button tictactoe;
        private System.Windows.Forms.Button close;
    }
}

