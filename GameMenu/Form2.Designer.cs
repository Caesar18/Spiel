﻿namespace GameMenu
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.close = new System.Windows.Forms.Button();
            this.player2 = new System.Windows.Forms.PictureBox();
            this.p2score = new System.Windows.Forms.Label();
            this.ball = new System.Windows.Forms.PictureBox();
            this.player1 = new System.Windows.Forms.PictureBox();
            this.p1score = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.player2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.player1)).BeginInit();
            this.SuspendLayout();
            // 
            // close
            // 
            this.close.Location = new System.Drawing.Point(722, 722);
            this.close.Name = "close";
            this.close.Size = new System.Drawing.Size(113, 37);
            this.close.TabIndex = 0;
            this.close.Text = "Schließen";
            this.close.UseVisualStyleBackColor = true;
            this.close.Click += new System.EventHandler(this.close_Click);
            // 
            // player2
            // 
            this.player2.BackColor = System.Drawing.Color.Red;
            this.player2.Location = new System.Drawing.Point(266, 12);
            this.player2.Name = "player2";
            this.player2.Size = new System.Drawing.Size(204, 24);
            this.player2.TabIndex = 1;
            this.player2.TabStop = false;
            this.player2.Click += new System.EventHandler(this.player2_Click);
            // 
            // p2score
            // 
            this.p2score.AutoSize = true;
            this.p2score.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2score.Location = new System.Drawing.Point(13, 18);
            this.p2score.Name = "p2score";
            this.p2score.Size = new System.Drawing.Size(31, 32);
            this.p2score.TabIndex = 2;
            this.p2score.Text = "0";
            // 
            // ball
            // 
            this.ball.BackColor = System.Drawing.Color.Black;
            this.ball.Location = new System.Drawing.Point(318, 174);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(35, 33);
            this.ball.TabIndex = 3;
            this.ball.TabStop = false;
            this.ball.Click += new System.EventHandler(this.ball_Click_1);
            // 
            // player1
            // 
            this.player1.BackColor = System.Drawing.Color.Blue;
            this.player1.Location = new System.Drawing.Point(266, 735);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(204, 24);
            this.player1.TabIndex = 4;
            this.player1.TabStop = false;
            // 
            // p1score
            // 
            this.p1score.AutoSize = true;
            this.p1score.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1score.Location = new System.Drawing.Point(12, 745);
            this.p1score.Name = "p1score";
            this.p1score.Size = new System.Drawing.Size(31, 32);
            this.p1score.TabIndex = 5;
            this.p1score.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(847, 771);
            this.Controls.Add(this.p1score);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.ball);
            this.Controls.Add(this.p2score);
            this.Controls.Add(this.player2);
            this.Controls.Add(this.close);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ping Pong";
            ((System.ComponentModel.ISupportInitialize)(this.player2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.player1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button close;
        private System.Windows.Forms.PictureBox player2;
        private System.Windows.Forms.Label p2score;
        private System.Windows.Forms.PictureBox ball;
        private System.Windows.Forms.PictureBox player1;
        private System.Windows.Forms.Label p1score;
        private System.Windows.Forms.Timer timer1;
    }
}