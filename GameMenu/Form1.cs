﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace GameMenu
{
    public partial class Form1 : Form
    {
        WindowsMediaPlayer player = new WindowsMediaPlayer();

        public Form1()
        {
            InitializeComponent();
            player.URL = "Game-Menu_Looping.mp3";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Möchten Sie das Menü wirklich schließen?", "Schließen", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            player.controls.play();
        }

        private void pingpong_Click(object sender, EventArgs e)
        {
            Form neuesFormular = new Form2();
            neuesFormular.Show(this);
        }

        private void tictactoe_Click(object sender, EventArgs e)
        {
            Form neuesFormular = new Form3();
            neuesFormular.Show(this);
        }
    }
}
